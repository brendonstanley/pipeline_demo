FROM python

ADD testScript.sh /
ADD timeOutDummy.sh /

WORKDIR /

RUN chmod +x testScript.sh
RUN chmod +x timeOutDummy.sh
#CMD /testScript.sh
CMD /timeOutDummy.sh
